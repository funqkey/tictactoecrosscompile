#ifndef WUERFELDISPLAY_H
#define WUERFELDISPLAY_H

class WuerfelDisplay {
  private:
    int displayConfig;
  public:
    WuerfelDisplay(int displayConfig) {
      // Use 'this->' to make the difference between the
      // 'value' attribute of the class and the
      // local variable 'value' created from the parameter.
      this->displayConfig = displayConfig;
    }
    int getValue() {
      return displayConfig;
    }
}; // don't forget the semicolon at the end of the class

#endif