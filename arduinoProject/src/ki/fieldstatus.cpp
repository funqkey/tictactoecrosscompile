#ifndef FIELDSTATUS_H
#define FIELDSTATUS_H

#include "player.cpp"

class FieldStatus {
  private:
      Player field[3][3][3] = {
		  {
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  },
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  },
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  }
		  },
		  {
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  },
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  },
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  }
		  },
		  {
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  },
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  },
			  {
			  Player(0),
			  Player(0),
			  Player(0)
			  }
		  }
		  
		  };
  public:
    FieldStatus() {
    }
    void set(int x,int y, int z, Player player){
        field[z][y][x] = player;
    }
    Player get(int x, int y, int z) {
        
        return field[z][y][x];
    }
};
#endif