#ifndef PLAYER_H
#define PLAYER_H

class Player {
  private:
    int value;
  public:
    Player(int value) {
      // Use 'this->' to make the difference between the
      // 'value' attribute of the class and the
      // local variable 'value' created from the parameter.
      this->value = value;
    }
    int getValue() {
      return value;
    }
}; // don't forget the semicolon at the end of the class

#endif