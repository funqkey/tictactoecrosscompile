#include <Adafruit_NeoPixel.h>
#include "src/tictactoe.cpp"
#include "src/ki/player.cpp"
#include "src/ki/fieldstatus.cpp"
#include "src/native/wuerfeldisplay.cpp"

#define NUMPIXELS 9
Adafruit_NeoPixel pixels1(NUMPIXELS, 2, NEO_GRB + NEO_KHZ800);

Adafruit_NeoPixel pixels2(NUMPIXELS, 4, NEO_GRB + NEO_KHZ800);

Adafruit_NeoPixel pixels3(NUMPIXELS, 6, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 200 // Time (in milliseconds) to pause between pixels



void printText(char* text){
  printf("%s",text);
  Serial.print(text);
}
void printInt(int zahl){
  printf("%i", zahl);
  Serial.print(zahl);
}

void setup() {
  pixels1.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels2.begin();
  pixels3.begin();

  Serial.begin(9600);
  Serial.print("Start");
}

int count = 0  ;
Player player(8);
FieldStatus fieldstatus;
WuerfelDisplay wuerfelDisplay(10);
TicTacToe tictactoe(wuerfelDisplay);

void loop() {

fieldstatus.set(1,1,1, player);
printInt(fieldstatus.get(1,1,1).getValue());
//printText(tictactoe.getMessage());
  
  //game.setInputs(readXyz(PIN3));
  //game.loop();
  //printChar(game.getMessage());
  //displayFieldStatus(game.getFieldStatus());
  
  pixels1.clear();
  pixels1.setPixelColor(count, pixels1.Color(55, 0, 0));
  pixels1.show();
  delay(DELAYVAL);
  pixels2.clear();
  pixels2.setPixelColor(count, pixels2.Color(0, 5,5));
  pixels2.show();
  count=(count+1)%NUMPIXELS;
  //printInt(player.getValue());
}
